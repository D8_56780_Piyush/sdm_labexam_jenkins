const express = require("express");
const app = express();
const cors = require("cors");

app.use(express.json());
app.use(cors("*"));

const routersbook = require("./routes/book");

app.use("/book", routersbook);

app.get("/", (request, response) => {
	response.send("WELCOME TO BOOK BACKEND SERVER API TEST");
});

app.listen(4000, "0.0.0.0", () => {
	console.log("SERVER STARTED ON POSRT 4000");
});
