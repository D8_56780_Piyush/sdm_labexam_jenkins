const {request, response} = require("express");
const express = require("express");
const router = express.Router();
const db = require("../db");
const utils = require("../utils");

// Display Book using the name via GET
router.get("/display", (request, response) => {
	const connection = db.openConnection();
	const {book_title} = request.body;
	const statement = `select * from book where book_title='${book_title}'`;

	connection.query(statement, (error, data) => {
		connection.end();
		if (error) {
			response.send(utils.createResult(error));
		} else {
			response.send(utils.createResult(error, data));
		}
	});
});


// Display all Books via GET
router.get("/displayall", (request, response) => {
	const connection = db.openConnection();
	const statement = `select * from book`;

	connection.query(statement, (error, data) => {
		connection.end();
		if (error) {
			response.send(utils.createResult(error));
		} else {
			response.send(utils.createResult(error, data));
		}
	});
});

// ADD Book data via POST
router.post("/add", (request, response) => {
    const connection = db.openConnection();
    const { book_title, publisher_name, author_name } = request.body;
    const statement = `insert into book (book_title, publisher_name, author_name) 
    values ('${book_title}','${publisher_name}','${author_name}');`;

	connection.query(statement, (error, data) => {
		connection.end();
		if (error) {
			response.send(utils.createResult(error));
		} else {
			response.send(utils.createResult(error, data));
		}
	});
});

// UPDATE publisher_name and Author_name using book id via PATCH

router.patch("/update", (request, response) => {
	const connection = db.openConnection();
	const {book_id, publisher_name, author_name} = request.body;
	const statement = `update book set publisher_name='${publisher_name}', author_name='${author_name}' where book_id = '${book_id}'`;

	connection.query(statement, (error, data) => {
		connection.end();
		if (error) {
			response.send(utils.createResult(error));
		} else {
			response.send(utils.createResult(error, data));
		}
	});
});

// DELETE book by book id via DELETE
router.delete("/delete", (request, response) => {
	const connection = db.openConnection();
	const {book_id} = request.body;
	const statement = `delete from book where book_id = '${book_id}'`;

	connection.query(statement, (error, data) => {
		connection.end();
		if (error) {
			response.send(utils.createResult(error));
		} else {
			response.send(utils.createResult(error, data));
		}
	});
});

module.exports = router;