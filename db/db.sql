CREATE TABLE book(
    book_id int primary key auto_increment, 
    book_title varchar(200), 
    publisher_name varchar(200), 
    author_name varchar(200)
);

INSERT INTO book (book_title, publisher_name, author_name) VALUES ("book1","publisher1","author1");
INSERT INTO book (book_title, publisher_name, author_name) VALUES ("book2","publisher2","author2");
INSERT INTO book (book_title, publisher_name, author_name) VALUES ("book3","publisher3","author3");
INSERT INTO book (book_title, publisher_name, author_name) VALUES ("book4","publisher4","author4");
